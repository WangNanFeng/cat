import React, { PureComponent } from "react"
import { ImagePicker, Toast, Modal, Picker, List } from "antd-mobile"
import { urlParse, fetchSimilarHeaders } from "../../util/util"
import { getRoomInfo, uploadEWM } from "../../api"
import { link } from "../../constants/conf"
import LSZP from "./img/room_title_lszp.png"
import QYLDS from "./img/room_title_qylds.png"
import LSTSW from "./img/room_title_lst.png"
import LLSW from "./img/room_title_llsw.png"
import DASW from "./img/room_title_dasw.png"
import logo from "./img/logo.png"
import ewmAreaTitle from "./img/ewmAreaTitle.png"
import "./index.scss"

const roomTypes = [
  undefined,
  undefined,
  LSZP,
  QYLDS,
  LSTSW,
  LLSW,
  undefined,
  DASW
]

const pickerData = [
  { label: 0.1, value: 0.1 },
  { label: 0.2, value: 0.2 },
  { label: 0.5, value: 0.5 },
  { label: 1, value: 1 },
  { label: 2, value: 2 },
  { label: 5, value: 5 }
]

class Room extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      state: undefined,
      pickerValue: localStorage.getItem("pickerValue")
        ? localStorage.getItem("pickerValue")
        : 1
    }
  }

  async componentDidMount() {
    const { location } = this.props
    const params = urlParse(location.search.split("?")[1])
    const { roomId } = params
    const headers = await fetchSimilarHeaders()
    const { mzphz_userId: userId } = headers
    if (!userId || !roomId) {
      window.location.href = link
      return
    }
    getRoomInfo(userId, roomId).then(res => {
      if (res.state !== 0) {
        window.location.href = link
      } else {
        const userImg = res.userInfos.filter(user => user.userId === userId)[0]
          .ewmUrl
        this.setState({ ...res, userId, roomId, files: [{ url: userImg }] })
      }
    })
  }

  onChange = files => {
    const { userId } = this.state

    if (files.length !== 0) {
      const file = files[0].file
      const { type, size } = file
      if (type !== "image/jpeg" && type !== "image/png") {
        Toast.info("只能上传jpg和png格式的图片")
        return
      }

      if (size / 1000 > 256) {
        Toast.info("图片大小不能大于256kb")
        return
      }
      const formData = new FormData()
      formData.append("userfile", file)
      formData.append("userId", userId)
      uploadEWM(formData).then(res => {
        const files = [
          { url: `${res.imgUrl}?${new Date().getTime()}`, id: Math.random() }
        ]
        this.setState({ files })
      })
    } else {
      this.setState({
        files
      })
    }
  }
  onOk = val => {
    const pickerValue = val[0]
    if (pickerValue === Number(this.state.pickerValue)) {
      return
    }
    localStorage.setItem("pickerValue", pickerValue)
    this.setState({ pickerValue })
  }

  getEwmList = () => {
    const { userInfos, userId, files, pickerValue } = this.state
    const { imageClick } = this
    return userInfos.map(user => (
      <li className="user-wrapper flex" key={user.userId}>
        <img src={user.headUrl} alt="" className="avatar" />
        <div className="user-info flex flex-column flex-justify-center">
          <span>{user.name}</span>
          <span>
            积分:{" "}
            {`${user.score > 0 ? "+" : ""}${(user.score * pickerValue).toFixed(
              1
            )}`}
          </span>
        </div>
        {user.userId === userId ? (
          <ImagePicker
            className="picker"
            files={files}
            onChange={this.onChange}
            length="1"
            selectable={files.length < 1}
            onImageClick={imageClick}
          />
        ) : (
          <ImagePicker
            className="picker"
            files={[{ url: user.ewmUrl }]}
            length="1"
            disableDelete={true}
            selectable={false}
            onImageClick={imageClick}
          />
        )}
      </li>
    ))
  }

  imageClick = (_index, files) => {
    const img = React.createElement("img", {
      src: files[0]["url"],
      alt: "",
      style: {
        width: "3rem",
        height: "auto"
      }
    })
    Modal.alert("", img)
  }

  render() {
    const { state, roomInfo, pickerValue } = this.state
    const { getEwmList, onOk } = this
    return (
      <div className="room-page">
        {state === 0 ? (
          <div className="room-content flex flex-column flex-align-center">
            <img src={logo} alt="" className="logo" />
            <img
              src={roomTypes[roomInfo.roomType]}
              alt=""
              className="room-type-img"
            />
            <p className="play-desc">房间玩法: {roomInfo.desc}</p>
            <img src={ewmAreaTitle} alt="" className="ewm-title" />
            <List>
              <Picker
                extra="低分"
                cols={1}
                value={[pickerValue * 1]}
                data={pickerData}
                onOk={onOk}
              >
                <List.Item arrow="horizontal">底分</List.Item>
              </Picker>
            </List>
            <ul className="ewm-wrapper flex flex-column">{getEwmList()}</ul>
            <div />
          </div>
        ) : null}
      </div>
    )
  }
}

export default Room
