import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import App from "./App"
import { is_weixn } from "./util/util"
import { link } from "./constants/conf"

const iswx = is_weixn()

if (!iswx) {
  window.location.href = link
} else {
  ReactDOM.render(<App />, document.getElementById("root"))

  if (module.hot) {
    module.hot.accept()
  }
}
