import { useEffect, useCallback } from 'react'
import useSetState from './useSetState'
const useAsync = (fn = () => Promise, args, defaultRes = {}) => {
  const [state, setState] = useSetState({
    loading: true,
    data: defaultRes,
    error: null
  })
  const memoized = useCallback(() => {
    return fn.apply(fn, args)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, args)

  useEffect(
    () => {
      let mounted = true
      setState({
        loading: true
      })
      const promise = memoized()

      promise
        .then(data => {
          if (mounted) {
            setState({
              loading: false,
              data,
              error: null
            })
          }
        })
        .catch(error => {
          if (mounted) {
            setState({
              loading: false,
              error
            })
          }
        })

      return () => {
        mounted = false
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [memoized]
  )

  return [state.loading, state.data, state.error]
}

export default useAsync
