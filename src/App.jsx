import React, { lazy, Suspense } from "react"
import { HashRouter as Router, Route, Switch, Redirect } from "react-router-dom"
import "./App.css"

const Room = lazy(() => import("./route/room"))
const QZ = lazy(() => import("./route/qz"))

function App() {
  return (
    <Suspense fallback={null}>
      <Router>
        <Switch>
          <Route path="/room" component={Room} />
          <Route path="/qz" component={QZ} />
          <Redirect to="/room"> </Redirect>
        </Switch>
      </Router>
    </Suspense>
  )
}

export default App
