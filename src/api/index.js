import request from "../http"
import { param } from "../util/util"
import { CODE_URL } from "../constants/conf"

// 获取房间信息接口
export const getRoomInfo = (roomId, userId) => {
  return new Promise(resolve => {
    request
      .get(
        `/getRoomInfo` +
          param({
            roomId,
            userId
          })
      )
      .then(res => {
        if (Array.isArray(res.userInfos) && res.userInfos.length !== 0) {
          res.userInfos = res.userInfos.map(user => {
            user.ewmUrl = `${CODE_URL}${user.ewmUrl}`
            return user
          })
        }
        resolve(res)
      })
      .catch(() => {
        resolve({})
      })
  })
}

// 上传二维码接口
export const uploadEWM = formData => {
  let config = {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  }
  return new Promise(resolve => {
    request
      .post(`/uploadEWM`, formData, config)
      .then(res => {
        res.imgUrl = `${CODE_URL}${res.imgUrl}`
        resolve(res)
      })
      .catch(() => {
        resolve({})
      })
  })
}

// 获取群主接口
export const getEWMImg = (userId, qId) => {
  return new Promise(resolve => {
    request
      .get(
        `/getEWMImg` +
          param({
            userId,
            qId
          })
      )
      .then(res => {
        res.ewmUrl = `${CODE_URL}${res.ewmUrl}`
        resolve(res)
      })
      .catch(() => {
        resolve({})
      })
  })
}
