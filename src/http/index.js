/**
 * axios封装
 * 请求拦截、响应拦截、错误统一处理
 */
import axios from "axios"
import { CAT_API } from "../constants/conf"

// 创建axios实例
const instance = axios.create({
  baseURL: CAT_API
})

// 设置post请求头
// instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

/**
 * 请求拦截器
 * 每次请求前，如果存在token则在请求头中携带token
 */
instance.interceptors.request.use(
  // 在发送请求之前做些什么
  config => {
    return config
  },
  error => {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 响应拦截器
instance.interceptors.response.use(
  // 请求成功
  res => {
    return res.status === 200 ? Promise.resolve(res.data) : Promise.reject(res)
  },
  // 请求已发出，但是不在2xx的范围
  error => {
    const { response } = error
    // 对响应错误做点什么
    if (response) {
      // errorHandle(response.status)
      return Promise.reject(error)
    }
  }
)

export default instance
